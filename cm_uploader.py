from threading import Thread
import time,datetime
import TelegramSender as tel
import TelebotTester as tbt

#메인쓰레드
class MainThread(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.delaySecount = 20
        self.EndFlag = False
        self.m_teletester = tbt.tel_bot_tester()
        self.m_telesenter = False
        #
        self.m_teletester.start()
        self.m_teletester.join()


    #실행부분
    def run(self):
        while self.EndFlag != True:            
            self.do_action()
            time.sleep(self.delaySecount)

    #모든쓰레드들이 정상작동하고 있는지를 검사하고 리턴 5분마다.
    def do_action(self):
        pass

if __name__ == '__main__':
    mt = MainThread()
    mt.do_Test(1)
    #mt.start()
    #mt.join()
