import telepot
import json
import threading
from threading import Thread
import time

ThreadLock = threading.Lock()
class tel_bot(Thread):
	def __init__(self):
		Thread.__init__(self)
		with open('load_id.json') as data_file:    
			self.users_data = json.load(data_file)
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			self.users_data[id_str]['inst'] = telepot.Bot(self.users_data[id_str]['token'])
			self.users_data[id_str]['inst'].message_loop(self.msg_handle)

		self.start()

	def run(self):
		while True:
			#get out queue and do act
			time.sleep(10)

	def msg_handle(self,msg):
		content_type, chat_type, chat_id = telepot.glance(msg)	#메시지에서 추출하는부분
		if content_type == 'text':
			#누가보낸건지 찾아냄
			target_bot = None
			for idx,md in enumerate(range(len(self.users_data))):
				id_str = 'user'+ str(idx+1)
				if str(self.users_data[id_str]['id']) == str(chat_id):
					target_bot = self.users_data[id_str]['inst']
			
			#여기서 부터 짜면됨 메시지는 str(msg['text'])
			#target_bot.sendMessage(chat_id, str(msg['text']) + ' 라는 니메세지 잘받음 ㅇㅇ')

	def sendMessage_alone(self,astr):
		ThreadLock.acquire()
		self.users_data['user1']['inst'].sendMessage(self.users_data['user1']['id'],astr)
		ThreadLock.release()

	def sendMessage_all(self,astr):
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			ThreadLock.acquire()
			self.users_data[id_str]['inst'].sendMessage(self.users_data[id_str]['id'],astr)
			ThreadLock.release()

	def sendPhoto_alone(self,aurl,amessage):
		ThreadLock.acquire()
		self.users_data['user1']['inst'].sendPhoto(self.users_data['user1']['id'],aurl,caption=amessage)
		ThreadLock.release()

	def sendPhoto_all(self,aurl,amessage):
		for idx,md in enumerate(range(len(self.users_data))):
			id_str = 'user'+ str(idx+1)
			ThreadLock.acquire()
			self.users_data[id_str]['inst'].sendPhoto(self.users_data[id_str]['id'],aurl,caption=amessage)
			ThreadLock.release()

if __name__ == '__main__':
	tel_bot()